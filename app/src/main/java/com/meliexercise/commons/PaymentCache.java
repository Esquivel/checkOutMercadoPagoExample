package com.meliexercise.commons;

import android.content.Context;

import com.meliexercise.domain.PaymentMethod;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Weeks;

import java.util.ArrayList;
import java.util.List;

public class PaymentCache {
	
	private static final int EXPIRATION_LIMIT_WEEK = 1;
	private PaymentSharedPreferences paymentSharedPreferences;
	
	public PaymentCache(Context context) {
		paymentSharedPreferences = new PaymentSharedPreferences(context);
	}
	
	public List<PaymentMethod> getPaymentsMethods() {
		expireDataIfNeeded();
		return paymentSharedPreferences.getPayments();
	}
	
	public void setPaymentsMethods(List<PaymentMethod> paymentsMethods) {
		paymentSharedPreferences.setPayments(paymentsMethods, getExpirationStringDate());
	}
	
	private String getExpirationStringDate() {
		return getNow().toString();
	}
	
	private void expireDataIfNeeded() {
		DateTime expirationDate = new DateTime(paymentSharedPreferences.getExpirationDate());
		
		int differenceInWeeks = Weeks.weeksBetween(expirationDate, new DateTime(getNow())).getWeeks();
		if (differenceInWeeks > EXPIRATION_LIMIT_WEEK) {
			paymentSharedPreferences.setPayments(new ArrayList<PaymentMethod>(), getExpirationStringDate());
		}
	}
	
	private DateTime getNow() {
		return new DateTime(LocalDate.now().toString("yyyy-MM-dd"));
	}
}
