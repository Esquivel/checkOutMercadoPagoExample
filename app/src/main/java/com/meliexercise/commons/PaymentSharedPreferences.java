package com.meliexercise.commons;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.meliexercise.domain.PaymentMethod;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PaymentSharedPreferences extends AbstractSharedPreferences {
	
	private static final String PAYMENT_SHARED_PREFERENCES = "payment_shared_preferences";
	private static final String PAYMENTS = "payments";
	private static final String EXPIRATION_DATE = "expiration_date";
	
	public PaymentSharedPreferences(Context context) {
		init(context, PAYMENT_SHARED_PREFERENCES);
	}
	
	public void setPayments(List<PaymentMethod> paymentMethods, String expirationDate) {
		Gson phrasesToJson = new Gson();
		String phrasesToSave = phrasesToJson.toJson(paymentMethods);
		getEditor().putString(PAYMENTS, phrasesToSave);
		getEditor().putString(EXPIRATION_DATE, expirationDate);
		applyChanges();
	}
	
	public List<PaymentMethod> getPayments(){
		Gson gson = new Gson();
		Type type = new TypeToken<List<PaymentMethod>>() {}.getType();
		return new ArrayList<>((List<PaymentMethod>)gson.fromJson(getPref().getString(PAYMENTS, new Gson().toJson(new ArrayList<PaymentMethod>())), type));
	}
	
	public String getExpirationDate() {
		return getPref().getString(EXPIRATION_DATE, null);
	}
}