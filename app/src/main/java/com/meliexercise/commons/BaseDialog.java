package com.meliexercise.commons;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class BaseDialog extends DialogFragment {
	
	public interface IDialogCreator {
		void onClickSubmit();
		void onClickCancel();
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (!(context instanceof IDialogCreator)) {
			throw new ClassCastException(getActivity().toString() + " must implement IDialogCreator");
		}
	}
	
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity())
				.setTitle("")
				.setMessage("")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((IDialogCreator) getActivity()).onClickSubmit();
					}
				})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((IDialogCreator)getActivity()).onClickCancel();
					}
				})
				.create();
	}
	
	@Override
	public void onDestroy() {
		this.dismiss();
		super.onDestroy();
	}
}