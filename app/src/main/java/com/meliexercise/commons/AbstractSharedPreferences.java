package com.meliexercise.commons;

import android.content.Context;
import android.content.SharedPreferences;

public class AbstractSharedPreferences {
	
	private static final int PRIVATE_MODE = 0;
	private SharedPreferences pref;
	private SharedPreferences.Editor editor;
	
	protected void init(Context context, String name) {
		pref = context.getSharedPreferences(name, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	protected void applyChanges() {
		editor.apply();
		editor.commit();
	}
	
	protected void clearSharedPreferences() {
		editor.clear();
		editor.commit();
	}
	
	protected SharedPreferences getPref() {
		return pref;
	}
	
	public SharedPreferences.Editor getEditor() {
		return editor;
	}
}