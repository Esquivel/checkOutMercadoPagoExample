package com.meliexercise.commons;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.meliexercise.R;

public class CheckOutDetailDialog extends BaseDialog {
	
	public AlertDialog detailDialog(Context context, String amount, String payment, String issuer, String quota) {
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		View view = layoutInflater.inflate(R.layout.check_out_detail_dialog, null);
		
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setView(view);
		alertDialog.setTitle(context.getString(R.string.dialog_title));
		
		TextView amountTextView = view.findViewById(R.id.amount);
		amountTextView.setText(context.getString(R.string.dialog_amount_description, amount));
		
		TextView paymentTextView = view.findViewById(R.id.payment);
		paymentTextView.setText(context.getString(R.string.dialog_payment_description, payment));
		
		TextView issuerTextView = view.findViewById(R.id.issuer);
		issuerTextView.setText(context.getString(R.string.dialog_issuer_description, issuer));
		
		TextView quotaTextView = view.findViewById(R.id.quota);
		quotaTextView.setText(context.getString(R.string.dialog_quota_description, quota));
		
		alertDialog.setNegativeButton(context.getString(R.string.dialog_close_text), null);

		return alertDialog.create();
	}
}
