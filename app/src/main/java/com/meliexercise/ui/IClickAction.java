package com.meliexercise.ui;

import com.meliexercise.domain.CheckOutData;

public interface IClickAction {
	
	void onClickNextButton(CheckOutData checkOutData);
	
}
