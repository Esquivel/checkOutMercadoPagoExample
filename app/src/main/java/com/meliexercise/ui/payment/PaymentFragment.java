package com.meliexercise.ui.payment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.commons.Constants;
import com.meliexercise.domain.CheckOutData;
import com.meliexercise.domain.PaymentMethod;
import com.meliexercise.service.PaymentService;
import com.meliexercise.ui.IClickAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaymentFragment extends Fragment implements PaymentService.IPaymentsCallback {
	
	private static final String PAYMENT_METHODS = "payment_methods";
	private IClickAction iClickAction;
	private PaymentService paymentService;
	private PaymentAdapter paymentAdapter;
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private TextView title;
	private TextView message;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			iClickAction = (IClickAction)context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement iClickAction");
		}
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		paymentService = PaymentService.getInstance();
		paymentAdapter = new PaymentAdapter(new PaymentAdapter.IPaymentAdapterCallback() {
			@Override
			public void onClickPaymentMethod(PaymentMethod paymentMethod) {
				CheckOutData checkOutData = (CheckOutData)getArguments().get(Constants.CHECK_OUT_DATA);
				checkOutData.setPaymentMethod(paymentMethod);
				iClickAction.onClickNextButton(checkOutData);
			}
		});
	}
	
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.default_list_fragment, container, false);
	}
	
	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		RecyclerView recyclerPaymentMethods = view.findViewById(R.id.recycler);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
		recyclerPaymentMethods.setLayoutManager(linearLayoutManager);
		recyclerPaymentMethods.setAdapter(paymentAdapter);
		
		title = view.findViewById(R.id.title);
		message = view.findViewById(R.id.message);
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			paymentMethods = (List<PaymentMethod>)savedInstanceState.get(PAYMENT_METHODS);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (paymentMethods.size() == 0) {
			onShowMessage(getString(R.string.default_loading));
			paymentService.getPaymentMethods(this, getContext(), true);
		} else {
			populateViews();
		}
	}
	
	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(PAYMENT_METHODS, (Serializable)paymentMethods);
	}
	
	@Override
	public void onPause() {
		paymentService.unsubscribe();
		super.onPause();
	}
	
	@Override
	public void onResponseOk(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
		populateViews();
	}
	
	@Override
	public void onResponseConnectionError() {
		onShowMessage(getString(R.string.default_connection_error_response));
	}
	
	@Override
	public void onResponseError() {
		onShowMessage(getString(R.string.default_error_response));
	}
	
	@Override
	public void onResponseNoData() {
		onShowMessage(getString(R.string.default_no_data_response));
	}
	
	@Override
	public void onResponseTimeOut() {
		onShowMessage(getString(R.string.default_time_out_response));
	}
	
	private void populateViews() {
		onLoadData();
	}
	
	private void onLoadData() {
		title.setVisibility(View.VISIBLE);
		title.setText(getString(R.string.payment_selector_title));
		message.setVisibility(View.GONE);
		paymentAdapter.addData(paymentMethods);
	}
	
	private void onShowMessage(String messageAlert) {
		title.setVisibility(View.GONE);
		message.setVisibility(View.VISIBLE);
		message.setText(messageAlert);
	}
}
