package com.meliexercise.ui.payment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.domain.PaymentMethod;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.PaymentItem> {
	
	public interface IPaymentAdapterCallback {
		void onClickPaymentMethod(PaymentMethod paymentMethod);
	}
	
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	private IPaymentAdapterCallback iPaymentAdapterCallback;
	
	public PaymentAdapter(IPaymentAdapterCallback iPaymentAdapterCallback) {
		this.iPaymentAdapterCallback = iPaymentAdapterCallback;
	}
	
	public PaymentItem onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.default_card_view_list_adapter, parent, false);
		return new PaymentItem(view);
	}
	
	@Override
	public void onBindViewHolder(PaymentItem holder, int position) {
		holder.name.setText(paymentMethods.get(position).getName());
		
		Picasso.with(holder.image.getContext())
				.load(paymentMethods.get(position).getThumbnail())
				.fit().centerInside()
				.placeholder(R.drawable.ic_place_holder_24px)
				.into(holder.image);
	}
	
	@Override
	public int getItemCount() {
		return paymentMethods.size();
	}
	
	public void addData(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = new ArrayList<>(paymentMethods);
		notifyDataSetChanged();
	}
	
	class PaymentItem extends RecyclerView.ViewHolder {
		
		ImageView image;
		TextView name;
		
		PaymentItem(View itemView) {
			super(itemView);
			image = itemView.findViewById(R.id.image);
			name = itemView.findViewById(R.id.name);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					iPaymentAdapterCallback.onClickPaymentMethod(paymentMethods.get(getAdapterPosition()));
				}
			});
		}
	}
}
