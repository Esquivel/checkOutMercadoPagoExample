package com.meliexercise.ui.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.meliexercise.R;
import com.meliexercise.commons.Constants;
import com.meliexercise.domain.CheckOutData;
import com.meliexercise.ui.IClickAction;
import com.meliexercise.ui.cardIssuer.CardIssuerActivity;

public class PaymentActivity extends AppCompatActivity implements IClickAction {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.default_activity_layout);
		
		if (savedInstanceState == null) {
			Bundle bundle = getIntent().getExtras();
			FragmentManager fragmentManager = getSupportFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			PaymentFragment amountFragment = new PaymentFragment();
			amountFragment.setArguments(bundle);
			fragmentTransaction.add(R.id.mainContainer, amountFragment);
			fragmentTransaction.commit();
		}
		
		setToolbar();
	}
	
	private void setToolbar() {
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle(getString(R.string.payment_selector_screen_name));
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.exit_start, R.anim.exit_finish);
	}
	
	private Class getNextActivity() {
		return CardIssuerActivity.class;
	}
	
	@Override
	public void onClickNextButton(CheckOutData checkOutData) {
		Intent intent = new Intent(this, getNextActivity());
		intent.putExtra(Constants.CHECK_OUT_DATA, checkOutData);
		startActivity(intent);
		overridePendingTransition(R.anim.enter_start, R.anim.enter_finish);
	}
}
