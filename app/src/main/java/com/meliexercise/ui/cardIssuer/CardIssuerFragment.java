package com.meliexercise.ui.cardIssuer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.commons.Constants;
import com.meliexercise.domain.CardIssuer;
import com.meliexercise.domain.CheckOutData;
import com.meliexercise.service.CardIssuerService;
import com.meliexercise.ui.IClickAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CardIssuerFragment extends Fragment implements CardIssuerService.ICardIssuerCallback {
	
	private static final String CARD_ISSUER_LIST = "card_issuer_list";
	private static final String ID_PAYMENT_METHOD = "id_payment_method";
	private IClickAction iClickAction;
	private CardIssuerService cardIssuerService;
	private List<CardIssuer> cardIssuerList = new ArrayList<>();
	private TextView title;
	private TextView message;
	private String idPaymentMethod;
	private CardIssuerAdapter cardIssuerAdapter;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			iClickAction = (IClickAction)context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement iClickAction");
		}
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cardIssuerService = CardIssuerService.getInstance();
		cardIssuerAdapter = new CardIssuerAdapter(new CardIssuerAdapter.ICardIssuerAdapterCallback() {
			@Override
			public void onClickIssuing(CardIssuer cardIssuer) {
				CheckOutData checkOutData = (CheckOutData)getArguments().getSerializable(Constants.CHECK_OUT_DATA);
				checkOutData.setCardIssuer(cardIssuer);
				iClickAction.onClickNextButton(checkOutData);
			}
		});
	}
	
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.default_list_fragment, container, false);
	}
	
	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		RecyclerView recyclerIssuingBank = view.findViewById(R.id.recycler);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
		recyclerIssuingBank.setLayoutManager(linearLayoutManager);
		recyclerIssuingBank.setAdapter(cardIssuerAdapter);
		
		title = view.findViewById(R.id.title);
		message = view.findViewById(R.id.message);
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			cardIssuerList = (List<CardIssuer>)savedInstanceState.get(CARD_ISSUER_LIST);
			idPaymentMethod = (String)savedInstanceState.get(ID_PAYMENT_METHOD);
		} else {
			CheckOutData checkOutData = (CheckOutData)getArguments().get(Constants.CHECK_OUT_DATA);
			idPaymentMethod = checkOutData.getPaymentMethod().getId();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (cardIssuerList.size() == 0) {
			onShowMessage(getString(R.string.default_loading));
			cardIssuerService.getCardIssuers(this, idPaymentMethod);
		} else {
			populateViews();
		}
	}
	
	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(ID_PAYMENT_METHOD, idPaymentMethod);
		outState.putSerializable(CARD_ISSUER_LIST, (Serializable)cardIssuerList);
	}
	
	@Override
	public void onPause() {
		cardIssuerService.unsubscribe();
		super.onPause();
	}
	
	@Override
	public void onResponseOk(List<CardIssuer> cardIssuers) {
		this.cardIssuerList = cardIssuers;
		populateViews();
	}
	
	@Override
	public void onResponseError() {
		onShowMessage(getString(R.string.default_error_response));
	}
	
	@Override
	public void onResponseNoData() {
		onShowMessage(getString(R.string.default_no_data_response));
	}
	
	@Override
	public void onResponseTimeOut() {
		onShowMessage(getString(R.string.default_time_out_response));
	}
	
	@Override
	public void onResponseConnectionError() {
		onShowMessage(getString(R.string.default_connection_error_response));
	}
	
	private void populateViews() {
		onLoadData();
	}
	
	private void onLoadData() {
		title.setVisibility(View.VISIBLE);
		title.setText(R.string.card_issuer_selector_title);
		message.setVisibility(View.GONE);
		cardIssuerAdapter.addData(cardIssuerList);
	}
	
	private void onShowMessage(String messageAlert) {
		title.setVisibility(View.GONE);
		message.setVisibility(View.VISIBLE);
		message.setText(messageAlert);
	}
}
