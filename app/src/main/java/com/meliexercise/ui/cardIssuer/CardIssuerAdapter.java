package com.meliexercise.ui.cardIssuer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.domain.CardIssuer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CardIssuerAdapter extends RecyclerView.Adapter<CardIssuerAdapter.IssuingItem> {
	
	public interface ICardIssuerAdapterCallback {
		void onClickIssuing(CardIssuer cardIssuer);
	}
	
	private List<CardIssuer> cardIssuers = new ArrayList<>();
	private ICardIssuerAdapterCallback iCardIssuerAdapterCallback;
	
	public CardIssuerAdapter(ICardIssuerAdapterCallback iCardIssuerAdapterCallback) {
		this.iCardIssuerAdapterCallback = iCardIssuerAdapterCallback;
	}
	
	public IssuingItem onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.default_card_view_list_adapter, parent, false);
		return new IssuingItem(view);
	}
	
	@Override
	public void onBindViewHolder(IssuingItem holder, int position) {
		holder.name.setText(cardIssuers.get(position).getName());

		Picasso.with(holder.image.getContext())
				.load(cardIssuers.get(position).getSecureThumbnail())
				.fit().centerInside()
				.placeholder(R.drawable.ic_place_holder_24px)
				.into(holder.image);
	}
	
	@Override
	public int getItemCount() {
		return cardIssuers.size();
	}
	
	public void addData(List<CardIssuer> cardIssuers) {
		this.cardIssuers = new ArrayList<>(cardIssuers);
		notifyDataSetChanged();
	}
	
	class IssuingItem extends RecyclerView.ViewHolder {
		
		ImageView image;
		TextView name;
		
		IssuingItem(View itemView) {
			super(itemView);
			image = itemView.findViewById(R.id.image);
			name = itemView.findViewById(R.id.name);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					iCardIssuerAdapterCallback.onClickIssuing(cardIssuers.get(getAdapterPosition()));
				}
			});
		}
	}
}