package com.meliexercise.ui.amount;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meliexercise.R;
import com.meliexercise.commons.CheckOutDetailDialog;
import com.meliexercise.commons.Constants;
import com.meliexercise.domain.CheckOutData;
import com.meliexercise.ui.IClickAction;

import java.math.BigDecimal;

public class AmountFragment extends Fragment {
	
	private static final float MIN_VALUE_AMOUNT = 100f;
	private static final float MAX_VALUE_AMOUNT = 99999f;
	private IClickAction iClickAction;
	private TextInputEditText amount;
	private TextInputLayout amountInputLayout;
	private CheckOutData checkOutData;
	private AlertDialog alertDialog;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			iClickAction = (IClickAction)context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement iClickAction");
		}
	}
	
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.amount_fragment, container, false);
	}
	
	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		amount = view.findViewById(R.id.amount);
		amountInputLayout = view.findViewById(R.id.amountInputLayout);
		FloatingActionButton buttonNext = view.findViewById(R.id.buttonNext);
		buttonNext.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isAmountValid()) {
					amountInputLayout.setErrorEnabled(false);
					iClickAction.onClickNextButton(createCheckOutData());
				}
			}
		});
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (getArguments() != null) {
			checkOutData = (CheckOutData)getArguments().getSerializable(Constants.CHECK_OUT_DATA);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (checkOutData != null) {
			CheckOutDetailDialog checkOutDetailDialog = new CheckOutDetailDialog();
			alertDialog = checkOutDetailDialog.detailDialog(
					getActivity(),
					String.valueOf(checkOutData.getAmount()),
					checkOutData.getPaymentMethod().getName(),
					checkOutData.getCardIssuer().getName(),
					checkOutData.getPayerCost().getRecommendedMessage());
			
			alertDialog.show();
		}
	}
	
	@Override
	public void onPause() {
		if (alertDialog != null && alertDialog.isShowing()) {
			alertDialog.dismiss();
		}
		super.onPause();
	}
	
	private boolean isAmountValid() {
		if (isAmountEmpty()) {
			showErrorMessage(getString(R.string.amount_empty_validation));
			return false;
		} else if (!isAmountValidRange()) {
			showErrorMessage(getString(R.string.amount_range_validation));
			return false;
		}
		return true;
	}
	
	private boolean isAmountEmpty() {
		return amount.getText() == null || amount.getText().toString().length() == 0;
	}
	
	private boolean isAmountValidRange() {
		Float amountHelper = Float.valueOf(this.amount.getText().toString());
		return amountHelper > MIN_VALUE_AMOUNT && amountHelper < MAX_VALUE_AMOUNT;
	}
	
	private void showErrorMessage(String message) {
		amountInputLayout.setErrorEnabled(true);
		amountInputLayout.setError(message);
	}
	
	private CheckOutData createCheckOutData() {
		CheckOutData checkOutData = new CheckOutData();
		checkOutData.setAmount(new BigDecimal(amount.getText().toString()));
		return checkOutData;
	}
}
