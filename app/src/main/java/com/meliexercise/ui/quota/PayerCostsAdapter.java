package com.meliexercise.ui.quota;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.domain.PayerCost;

import java.util.ArrayList;
import java.util.List;

class PayerCostsAdapter extends RecyclerView.Adapter<PayerCostsAdapter.PayerCostItem> {
	
	public interface IPayerCostAdapterCallback {
		void onClickPayerCost(PayerCost payerCost);
	}
	
	private List<PayerCost> payerCosts = new ArrayList<>();
	private IPayerCostAdapterCallback iPayerCostAdapterCallback;
	
	public PayerCostsAdapter(IPayerCostAdapterCallback iPayerCostAdapterCallback) {
		this.iPayerCostAdapterCallback = iPayerCostAdapterCallback;
	}
	
	public PayerCostItem onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payer_cost_adapter, parent, false);
		return new PayerCostItem(view);
	}
	
	@Override
	public void onBindViewHolder(PayerCostItem holder, int position) {
		holder.description.setText(payerCosts.get(position).getRecommendedMessage());
	}
	
	@Override
	public int getItemCount() {
		return payerCosts.size();
	}
	
	public void addData(List<PayerCost> payerCosts) {
		this.payerCosts = new ArrayList<>(payerCosts);
		notifyDataSetChanged();
	}
	
	class PayerCostItem extends RecyclerView.ViewHolder {
		
		TextView description;
		
		PayerCostItem(View itemView) {
			super(itemView);
			description = itemView.findViewById(R.id.description);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					iPayerCostAdapterCallback.onClickPayerCost(payerCosts.get(getAdapterPosition()));
				}
			});
		}
	}
}
