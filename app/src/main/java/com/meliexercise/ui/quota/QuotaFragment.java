package com.meliexercise.ui.quota;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meliexercise.R;
import com.meliexercise.commons.Constants;
import com.meliexercise.domain.CheckOutData;
import com.meliexercise.domain.PayerCost;
import com.meliexercise.domain.QuotaPlan;
import com.meliexercise.service.QuotaService;
import com.meliexercise.ui.IClickAction;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuotaFragment extends Fragment implements QuotaService.IQuotaCallback {
	
	private static final String QUOTA_PLAN = "quota_plan";
	private IClickAction iClickAction;
	private QuotaService quotaService;
	private List<QuotaPlan> quotaPlans = new ArrayList<>();
	private TextView title;
	private TextView message;
	private PayerCostsAdapter payerCostsAdapter;
	private CheckOutData checkOutData;
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			iClickAction = (IClickAction)context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement iClickAction");
		}
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		quotaService = QuotaService.getInstance();
		payerCostsAdapter = new PayerCostsAdapter(new PayerCostsAdapter.IPayerCostAdapterCallback() {
			@Override
			public void onClickPayerCost(PayerCost payerCost) {
				checkOutData.setPayerCost(payerCost);
				iClickAction.onClickNextButton(checkOutData);
			}
		});
	}
	
	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.default_list_fragment, container, false);
	}
	
	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		RecyclerView recyclerPaymentMethods = view.findViewById(R.id.recycler);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
		recyclerPaymentMethods.setLayoutManager(linearLayoutManager);
		recyclerPaymentMethods.setAdapter(payerCostsAdapter);
		
		title = view.findViewById(R.id.title);
		message = view.findViewById(R.id.message);
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			quotaPlans = (List<QuotaPlan>)savedInstanceState.get(QUOTA_PLAN);
		}
		checkOutData = (CheckOutData)getArguments().get(Constants.CHECK_OUT_DATA);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (quotaPlans.size() == 0) {
			onShowMessage(getString(R.string.default_loading));
			quotaService.getCardIssuers(this, checkOutData.getAmount(), checkOutData.getPaymentMethod().getId(),
					checkOutData.getCardIssuer().getId());
		} else {
			populateViews();
		}
	}
	
	@Override
	public void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(QUOTA_PLAN, (Serializable)quotaPlans);
	}
	
	@Override
	public void onPause() {
		quotaService.unsubscribe();
		super.onPause();
	}
	
	@Override
	public void onResponseConnectionError() {
		onShowMessage(getString(R.string.default_connection_error_response));
	}
	
	@Override
	public void onResponseError() {
		onShowMessage(getString(R.string.default_error_response));
	}
	
	@Override
	public void onResponseNoData() {
		onShowMessage(getString(R.string.default_no_data_response));
	}
	
	@Override
	public void onResponseTimeOut() {
		onShowMessage(getString(R.string.default_time_out_response));
	}
	
	@Override
	public void onResponseOk(List<QuotaPlan> quotaPlans) {
		this.quotaPlans = quotaPlans;
		populateViews();
	}
	
	private void populateViews() {
		onLoadData();
	}
	
	private void onLoadData() {
		title.setVisibility(View.VISIBLE);
		title.setText(getString(R.string.quota_issuer_selector_title));
		message.setVisibility(View.GONE);
		payerCostsAdapter.addData(quotaPlans.get(0).getPayerCosts());
	}
	
	private void onShowMessage(String messageAlert) {
		title.setVisibility(View.GONE);
		message.setVisibility(View.VISIBLE);
		message.setText(messageAlert);
	}
}
