package com.meliexercise.service;

import com.meliexercise.domain.QuotaPlan;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QuotaService extends AbstractService {
	
	public interface IQuotaCallback extends IServiceCallback {
		void onResponseOk(List<QuotaPlan> quotaPlans);
	}
	
	private IQuotaCallback iQuotaCallback;
	private IEndpointManager iEndpointManager;
	private static QuotaService instance;
	
	public static QuotaService getInstance() {
		if (instance == null) {
			instance = new QuotaService();
		}
		return instance;
	}
	
	private QuotaService() {
		init();
	}
	
	@Override
	protected void init() {
		iEndpointManager = get();
	}
	
	public void getCardIssuers(IQuotaCallback iQuotaCallback, BigDecimal amount, String paymentMethodId,
							   int issuerId) {
		this.iQuotaCallback = iQuotaCallback;
		doRequest(amount, paymentMethodId, issuerId);
	}
	
	private void doRequest(BigDecimal amount, String paymentMethodId, int issuerId) {
		Observable<List<QuotaPlan>> call = iEndpointManager.getQuotaAvailable(getPublicKey(), amount, paymentMethodId, issuerId);
		setSubscription(call.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
				.subscribe(createSubscriberGetQuotaAvailable()));
	}
	
	private Subscriber<List<QuotaPlan>> createSubscriberGetQuotaAvailable() {
		return new Subscriber<List<QuotaPlan>>() {
			@Override
			public void onCompleted() {
				//Nothing to do
			}
			
			@Override
			public void onError(Throwable e) {
				if (e instanceof HttpException) {
					HttpException response = (HttpException)e;
					int code = response.code();
					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						iQuotaCallback.onResponseNoData();
					} else if (code == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
						iQuotaCallback.onResponseTimeOut();
					} else {
						iQuotaCallback.onResponseError();
					}
				} else {
					iQuotaCallback.onResponseConnectionError();
				}
			}
			
			@Override
			public void onNext(List<QuotaPlan> quotaPlans) {
				if (quotaPlans.size() > 0 && quotaPlans.get(0).getPayerCosts().size() > 0) {
					iQuotaCallback.onResponseOk(quotaPlans);
				} else {
					iQuotaCallback.onResponseNoData();
				}
			}
		};
	}
	
	public void unsubscribe() {
		unsubscribeService();
	}
}
