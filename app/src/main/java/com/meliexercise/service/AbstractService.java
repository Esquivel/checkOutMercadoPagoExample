package com.meliexercise.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Subscription;

public abstract class AbstractService {
			
	private static final String BASE_URL = "https://api.mercadopago.com";
	private static final String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
	private static IEndpointManager iEndpointManager;
	private Subscription subscription;
	
	protected abstract void init();
	
	protected IEndpointManager get() {
		if (iEndpointManager == null) {
			createInstance();
		}
		return iEndpointManager;
	}
	
	private void createInstance() {
		iEndpointManager = new Retrofit
				.Builder()
				.baseUrl(BASE_URL)
				.client(setClientTimeOut(20))
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build()
				.create(IEndpointManager.class);
	}
	
	private OkHttpClient setClientTimeOut(int timeOut) {
		return new OkHttpClient.Builder()
				.readTimeout(timeOut, TimeUnit.SECONDS)
				.connectTimeout(timeOut, TimeUnit.SECONDS)
				.build();
	}
	
	protected void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}
	
	protected void unsubscribeService() {
		if (subscription != null) {
			subscription.unsubscribe();
		}
	}
	
	public static String getPublicKey() {
		return PUBLIC_KEY;
	}
}
