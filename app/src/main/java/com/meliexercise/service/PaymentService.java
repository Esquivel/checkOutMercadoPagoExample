package com.meliexercise.service;

import android.content.Context;

import com.meliexercise.commons.PaymentCache;
import com.meliexercise.domain.PaymentMethod;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PaymentService extends AbstractService {
	
	public interface IPaymentsCallback extends IServiceCallback {
		void onResponseOk(List<PaymentMethod> paymentMethods);
	}
	
	private IPaymentsCallback iPaymentsCallback;
	private IEndpointManager iEndpointManager;
	private static PaymentService instance;
	private PaymentCache paymentCache;
	
	public static PaymentService getInstance() {
		if (instance == null) {
			instance = new PaymentService();
		}
		return instance;
	}
	
	private PaymentService() {
		init();
	}
	
	@Override
	protected void init() {
		iEndpointManager = get();
	}
	
	public void getPaymentMethods(IPaymentsCallback iPaymentsCallback, Context context, boolean isCacheEnabled) {
		this.iPaymentsCallback = iPaymentsCallback;
		initCacheIfNeeded(context);
		
		if (isCacheEnabled) {
			getDataFromCache();
		} else {
			doRequest();
		}
	}
	
	private void initCacheIfNeeded(Context context) {
		if (paymentCache == null) {
			paymentCache = new PaymentCache(context);
		}
	}
	
	private void getDataFromCache() {
		List<PaymentMethod> paymentMethods = paymentCache.getPaymentsMethods();
		if (paymentMethods.size() > 0) {
			iPaymentsCallback.onResponseOk(paymentMethods);
		} else {
			doRequest();
		}
		
	}
	
	private void doRequest() {
		Observable<List<PaymentMethod>> call = iEndpointManager.getPaymentMethods(getPublicKey());
		setSubscription(call.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
				.subscribe(createSubscriberGetPaymentMethod()));
	}
	
	private Subscriber<List<PaymentMethod>> createSubscriberGetPaymentMethod() {
		return new Subscriber<List<PaymentMethod>>() {
			@Override
			public void onCompleted() {
				//Nothing to do
			}
			
			@Override
			public void onError(Throwable e) {
				if (e instanceof HttpException) {
					HttpException response = (HttpException)e;
					int code = response.code();
					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						iPaymentsCallback.onResponseNoData();
					} else if (code == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
						iPaymentsCallback.onResponseTimeOut();
					} else {
						iPaymentsCallback.onResponseError();
					}
				} else {
					iPaymentsCallback.onResponseConnectionError();
				}
			}
			
			@Override
			public void onNext(List<PaymentMethod> paymentMethods) {
				if (paymentMethods.size() > 0) {
					paymentCache.setPaymentsMethods(paymentMethods);
					iPaymentsCallback.onResponseOk(paymentMethods);
				} else {
					iPaymentsCallback.onResponseNoData();
				}
			}
		};
	}
	
	public void unsubscribe() {
		unsubscribeService();
	}
}