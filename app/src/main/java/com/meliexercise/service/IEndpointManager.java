package com.meliexercise.service;

import com.meliexercise.domain.CardIssuer;
import com.meliexercise.domain.PaymentMethod;
import com.meliexercise.domain.QuotaPlan;

import java.math.BigDecimal;
import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IEndpointManager {
	
	@GET("v1/payment_methods")
	Observable<List<PaymentMethod>> getPaymentMethods(@Query("public_key") String publicKey);
	
	@GET("v1/payment_methods/card_issuers")
	Observable<List<CardIssuer>> getCardIssuers(@Query("public_key") String publicKey,
												@Query("payment_method_id") String paymentMethodId);
	
	
	@GET("v1/payment_methods/installments")
	Observable<List<QuotaPlan>> getQuotaAvailable(@Query("public_key") String publicKey,
											@Query("amount") BigDecimal amount,
											@Query("payment_method_id") String paymentMethodId,
											@Query("issuer.id") int issuerId);
}
