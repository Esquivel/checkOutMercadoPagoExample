package com.meliexercise.service;

import com.meliexercise.domain.CardIssuer;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CardIssuerService extends AbstractService {
	
	public interface ICardIssuerCallback extends IServiceCallback {
		void onResponseOk(List<CardIssuer> cardIssuers);
	}
	
	private ICardIssuerCallback iCardIssuerCallback;
	private IEndpointManager iEndpointManager;
	private static CardIssuerService instance;
	
	public static CardIssuerService getInstance() {
		if (instance == null) {
			instance = new CardIssuerService();
		}
		return instance;
	}
	
	private CardIssuerService() {
		init();
	}
	
	@Override
	protected void init() {
		iEndpointManager = get();
	}
	
	public void getCardIssuers(ICardIssuerCallback iCardIssuerCallback, String idPaymentMethod) {
		this.iCardIssuerCallback = iCardIssuerCallback;
		doRequest(idPaymentMethod);
	}
	
	private void doRequest(String idPaymentMethod) {
		Observable<List<CardIssuer>> call = iEndpointManager.getCardIssuers(getPublicKey(), idPaymentMethod);
		setSubscription(call.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
				.subscribe(createSubscriberGetIssuingBank()));
	}
	
	private Subscriber<List<CardIssuer>> createSubscriberGetIssuingBank() {
		return new Subscriber<List<CardIssuer>>() {
			@Override
			public void onCompleted() {
				//Nothing to do
			}
			
			@Override
			public void onError(Throwable e) {
				if (e instanceof HttpException) {
					HttpException response = (HttpException)e;
					int code = response.code();
					if (code == HttpURLConnection.HTTP_NOT_FOUND) {
						iCardIssuerCallback.onResponseNoData();
					} else if (code == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {
						iCardIssuerCallback.onResponseTimeOut();
					} else {
						iCardIssuerCallback.onResponseError();
					}
				} else {
					iCardIssuerCallback.onResponseConnectionError();
				}
			}
			
			@Override
			public void onNext(List<CardIssuer> cardIssuers) {
				if (cardIssuers.size() > 0) {
					iCardIssuerCallback.onResponseOk(cardIssuers);
				} else {
					iCardIssuerCallback.onResponseNoData();
				}
			}
		};
	}
	
	public void unsubscribe() {
		unsubscribeService();
	}
}
