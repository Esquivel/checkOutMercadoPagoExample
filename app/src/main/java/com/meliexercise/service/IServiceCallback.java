package com.meliexercise.service;

public interface IServiceCallback {
	
	void onResponseNoData();
	
	void onResponseError();
	
	void onResponseTimeOut();
	
	void onResponseConnectionError();
	
}
