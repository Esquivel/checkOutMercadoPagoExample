package com.meliexercise.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentMethod implements Serializable {
	
	private String id;
	private String name;
	@SerializedName("payment_type_id")
	private String paymentTypeId;
	private String status;
	@SerializedName("secure_thumbnail")
	private String secureThumbnail;
	private String thumbnail;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getSecureThumbnail() {
		return secureThumbnail;
	}
	
	public void setSecureThumbnail(String secureThumbnail) {
		this.secureThumbnail = secureThumbnail;
	}
	
	public String getThumbnail() {
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
}
