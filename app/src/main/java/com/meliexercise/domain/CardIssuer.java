package com.meliexercise.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardIssuer implements Serializable{
	
	private int id;
	private String name;
	private String thumbnail;
	@SerializedName("secure_thumbnail")
	private String secureThumbnail;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getThumbnail() {
		return thumbnail;
	}
	
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public String getSecureThumbnail() {
		return secureThumbnail;
	}
	
	public void setSecureThumbnail(String secureThumbnail) {
		this.secureThumbnail = secureThumbnail;
	}
}
