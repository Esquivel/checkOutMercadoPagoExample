package com.meliexercise.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class CheckOutData implements Serializable {
	
	private BigDecimal amount;
	private PaymentMethod paymentMethod;
	private CardIssuer cardIssuer;
	private PayerCost payerCost;
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public CardIssuer getCardIssuer() {
		return cardIssuer;
	}
	
	public void setCardIssuer(CardIssuer cardIssuer) {
		this.cardIssuer = cardIssuer;
	}
	
	public PayerCost getPayerCost() {
		return payerCost;
	}
	
	public void setPayerCost(PayerCost payerCost) {
		this.payerCost = payerCost;
	}
}
