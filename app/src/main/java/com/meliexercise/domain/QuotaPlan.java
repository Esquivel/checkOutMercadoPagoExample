package com.meliexercise.domain;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class QuotaPlan implements Serializable {
	
	@SerializedName("issuer")
	private CardIssuer cardIssuer;
	@SerializedName("payer_costs")
	private List<PayerCost> payerCosts;
	
	public CardIssuer getCardIssuer() {
		return cardIssuer;
	}
	
	public void setCardIssuer(CardIssuer cardIssuer) {
		this.cardIssuer = cardIssuer;
	}
	
	public List<PayerCost> getPayerCosts() {
		return payerCosts;
	}
	
	public void setPayerCosts(List<PayerCost> payerCosts) {
		this.payerCosts = payerCosts;
	}
}
